import React from "react";

const Total = props => {
    const { total } = props
    return (
        <div>
            <p>Number of exercises {total}</p>
        </div>
    );
}

export default Total;
