import React from "react";
import Part from '../content/Part'

const Content = props => {
    const {part, exercises} = props
    return (
        <div>
            <Part part={part} exercises={exercises}/>
        </div>
    );
}

export default Content;
