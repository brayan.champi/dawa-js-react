import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/Header'
import Content from './components/content/Content'
import Total from './components/Total'


const App = () => {
    const course = {
        name: 'Half Stack application development',
        parts: [
            {
                name: 'Fundamentals of React',
                exercises: 10
            },
            {
                name: 'Using props to pass data',
                exercises: 7
            },
            {
                name: 'State of a component',
                exercises: 14
            }
        ]
    }

    const dataContent = course.parts.map((item) => {
        return <Content part={item.name} exercises={item.exercises}/>
    })

    const total = course.parts.reduce((prev, next) => {
        return prev + next.exercises
    }, 0)

    return (
        <div>
            <Header course={course.name}/>
            {dataContent}
            <Total total={total}/>
        </div>
    )
}

ReactDOM.render(<App/>, document.getElementById('root'))
